const path = require('path')
const nodeExternals = require('webpack-node-externals')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const WebpackHookPlugin = require('webpack-hook-plugin').default

const dev = process.env.NODE_ENV !== 'production'

module.exports = {
  mode: dev ? 'development' : 'production',
  entry: {
    server: './src/index.js',
  },
  output: {
    path: path.join(__dirname, './build/'),
    publicPath: '/',
    filename: 'index.js',
  },
  target: 'node',
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              [
                'next/babel',
                {
                  'preset-env': {
                    modules: 'commonjs',
                  },
                },
              ],
            ],
          },
        },
      },
    ],
  },
  node: {
    __dirname: false,
    __filename: false,
  },
  externals: [nodeExternals()],
  plugins: [
    ...(dev
      ? [
          new WebpackHookPlugin({
            onBuildEnd: [
              'nodemon --require dotenv-flow/config --watch ./build/index.js ./build/index.js',
            ],
          }),
        ]
      : [new CleanWebpackPlugin()]),
  ],
}
