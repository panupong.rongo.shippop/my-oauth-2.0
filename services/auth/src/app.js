import path from 'path'
import nextJS from 'next'
import getConfig from 'next/config'
import express from 'express'
import favicon from 'serve-favicon'
import helmet from 'helmet'
import cors from 'cors'

import testSeverRoute from '@routes'
import firstMiddleware from '@middlewares'

import { init as intSql } from '@database/sql'
import { initRedis } from '@database/redis'

import controllers from '@controllers'

export const init = async () => {
  const dev = process.env.NODE_ENV !== 'production'
  const nextApp = nextJS({ dev })

  const handle = nextApp.getRequestHandler()
  await Promise.all([
    // intSql(process.env.SQL_URL), // <---- sql database
    // initRedis(), // <---- redis
    nextApp.prepare(), // <---- nextjs
  ])

  const app = express()

  // middlewares
  app.disable('etag')
  app.use(favicon(path.join(__dirname, '../public/favicon.ico')))
  app.use(helmet())
  app.use(cors({ origin: '*', credentials: true }))

  // body and cookie parser
  app.use(express.json())
  app.use(express.urlencoded({ extended: true }))

  app.get('/status', (req, res) => {
    return res.send({ message: 'OK' })
  })

  app.use('/api', firstMiddleware, testSeverRoute)
  app.use('/api/controllers', controllers)

  app.get('*', (req, res) => handle(req, res))

  return app
}
