import React from 'react'
import clsx from 'clsx'
import { Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
  fontOption: {
    fontFamily: 'Kanit !important',
  },
  font: {
    color: 'red',
  },
})

const MyTypo = (props) => {
  const { fontOption = false, ...resProps } = props
  const classes = useStyles()

  return (
    <Typography
      className={clsx(classes.font, fontOption && classes.fontOption)}
      {...resProps}
    />
  )
}

export default MyTypo
