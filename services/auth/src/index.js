import * as app from '@src/app'
import getConfig from 'next/config'

app
  .init()
  .then((server) => {
    const { serverRuntimeConfig } = getConfig()
    server.listen(serverRuntimeConfig.app.port, () => {
      console.info(
        `> Server is running on Port: ${serverRuntimeConfig.app.port}`,
      )
    })
  })
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
