const middlewares = async (req, res, next) => {
  try {
    console.info(`1st middlewares`)
    return next()
  } catch (err) {
    return res.status(500).send({ message: 'middlewares error' })
  }
}

export default middlewares
