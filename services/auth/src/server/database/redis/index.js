/* eslint-disable import/no-mutable-exports */
import redis from 'redis'
import { promisify } from 'util'

let clientRedis = null
let delRedisAsync = null
let scanRedisAsync = null
let getRedisAsync = null
let setRedisAsync = null
let hmsetAsync = null
let hgetallAsync = null
let expiredAsync = null

const initRedis = () => {
  clientRedis = redis.createClient({
    url: process.env.REDIS_URL,
  })

  clientRedis.on('ready', () => {
    console.info(`Redis Ready :)`)
  })

  clientRedis.on('error', (err) => {
    console.error(`Redis Error -`, err)
  })
  delRedisAsync = promisify(clientRedis.del).bind(clientRedis)
  scanRedisAsync = promisify(clientRedis.scan).bind(clientRedis)
  getRedisAsync = promisify(clientRedis.get).bind(clientRedis)
  setRedisAsync = promisify(clientRedis.set).bind(clientRedis)
  hmsetAsync = promisify(clientRedis.hmset).bind(clientRedis)
  hgetallAsync = promisify(clientRedis.hgetall).bind(clientRedis)
  expiredAsync = promisify(clientRedis.expire).bind(clientRedis)
}

export {
  initRedis,
  clientRedis,
  delRedisAsync,
  scanRedisAsync,
  getRedisAsync,
  setRedisAsync,
  hmsetAsync,
  hgetallAsync,
  expiredAsync,
}
