import Sequelize from 'sequelize'
import { URL } from 'url'

const database = {
  models: {},
  sequelize: null,
}
export default database
// export const { models, sequelize } = database

export const getModels = () => {
  const { models } = database
  return { ...models }
}

export const getSequelize = () => {
  const { sequelize } = database
  return sequelize
}

export const init = async (url, sequelizeOptions = {}) => {
  console.info('url', url)
  const dialect = url.includes(`mysql:`) ? `mysql` : `mariadb`

  const urlObj = new URL(url)

  const dialectOptions = {
    multipleStatements: true,
    // autoJsonMap: false,
  }
  if (dialect === 'mariadb') {
    dialectOptions.autoJsonMap = false
  }

  const sequelize = new Sequelize(
    urlObj.pathname.split('/')[1],
    urlObj.username,
    urlObj.password,
    {
      dialectOptions,
      host: urlObj.hostname,
      port: urlObj.port,
      dialect,
      define: {
        charset: 'utf8mb4',
        freezeTableName: true,
      },
      pool: {
        max: 5 * 2,
        acquire: 60000 * 2,
      },
      logging: false,
      ...sequelizeOptions,
    },
  )

  const models = {}
  // const models = {
  //   Member: Member.init(sequelize),
  //   SSOToken: SSOToken.init(sequelize),
  //   Client: Client.init(sequelize),
  //   ApiKey: ApiKey.init(sequelize),
  //   EmailToken: EmailToken.init(sequelize),
  //   OtpToken: OtpToken.init(sequelize),
  //   ReceiptInfo: ReceiptInfo.init(sequelize),
  //   LogMigrateAction: LogMigrateAction.init(sequelize),
  //   MemberConfirmedEmail: MemberConfirmedEmail.init(sequelize),
  //   MemberVerifiedPhone: MemberVerifiedPhone.init(sequelize),
  //   MemberIdentityId: MemberIdentityId.init(sequelize),
  //   MemberIdentityLog: MemberIdentityLog.init(sequelize),
  //   MemberLockedId: MemberLockedId.init(sequelize),
  //   MemberLockedIdLog: MemberLockedIdLog.init(sequelize),
  //   Admin: Admin.init(sequelize),
  //   MigrateMemberFacbookMY: MigrateMemberFacbookMY.init(sequelize),
  //   MemberBindServices: MemberBindServices.init(sequelize),
  //   MemberServiceDocument: MemberServiceDocument.init(sequelize),
  //   MemberConsensPdpa: MemberConsensPdpa.init(sequelize),
  //   MemberBankAccount: MemberBankAccount.init(sequelize),
  //   LogMemberLogin: LogMemberLogin.init(sequelize),
  //   MarketplaceJoinVerifyPage: MarketplaceJoinVerifyPage.init(sequelize),
  //   Referral: Referral.init(sequelize),
  //   ReferralHistory: ReferralHistory.init(sequelize),
  // }

  // Object.values(models)
  //   .filter((model) => {
  //     return typeof model.associate === 'function'
  //   })
  //   .forEach((model) => model.associate(models))

  if (process.env.SYNC_DB === 'true') await sequelize.sync()

  database.models = models
  database.sequelize = sequelize
  console.info(``)
  console.info(`----------------- sql checkup --------------------`)
  console.info(`---------> init sql dabase success <---------`)
  console.info(`--------------------------------------------------`)
  console.info(``)

  return { models, sequelize }
}

// export const resetDb = async (errorMessage = '', scene = ``) => {
//   try {
//     const CONDITION = [
//       'SequelizeConnectionAcquireTimeoutError'.toUpperCase(),
//       'SequelizeConnectionRefusedError'.toUpperCase(),
//       'SequelizeConnectionTimedOutError'.toUpperCase(),
//     ]
//     const isReset = CONDITION.includes(errorMessage.toUpperCase())
//     if (isReset) {
//       const currentSequelize = getSequelize()
//       let status1 = `close`
//       try {
//         await currentSequelize.authenticate()
//         status1 = `open`
//       } catch (err1) {
//         status1 = `close`
//       }
//       console.info(`before currentSequelize`, status1)
//       await currentSequelize.close()
//       console.info(`close base`)
//       let status2 = `close`
//       try {
//         await currentSequelize.authenticate()
//         status2 = `open`
//       } catch (err2) {
//         status2 = `close`
//       }
//       // MYSQL_URL
//       console.info(`after currentSequelize`, status2)
//       await init(process.env.MYSQL_URL)

//       return true
//     }
//     return false
//   } catch (err) {
//     console.info(`database/mysql`, err.message)
//     return false
//   }
// }
