import express from 'express'

const router = express.Router()

router.get('/status', (req, res) => {
  return res.send({ message: 'OK' })
})

export default router
