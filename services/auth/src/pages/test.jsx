import React from 'react'
import Component from '@components'
import Container from '@containers'
import MyTypo from '@components/MyTypo'

const test = () => {
  return (
    <div>
      <h1>test</h1>
      <Component />
      <Container />
      <MyTypo variant="h1" component="h2">
        MyTypo
      </MyTypo>
      <MyTypo variant="h1" component="h5" fontOption>
        MyTypo
      </MyTypo>
    </div>
  )
}

export default test
