module.exports = {
  // exportTrailingSlash: true,
  serverRuntimeConfig: {
    app: {
      env: process.env.ENV,
      port: process.env.NODE_PORT,
      sqlUrl: process.env.SQL_URL,
    },
  },
  publicRuntimeConfig: {},
  distDir: 'build/.next',
}
